package com.Inventory;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class InventoryTest {
    Inventory inventory;
    Country brazil;
    Country argentina;

    @Before
    public void setup() {
        inventory = new Inventory();
        brazil = new Country("Brazil", 100, 100, inventory);
        inventory.addCountry(brazil);
        argentina = new Country("Argentina", 100, 50, inventory);
        inventory.addCountry(argentina);


    }

    @Test
    public void shouldBeAbleToGetInventoryFromBrazil() {
        assertEquals("500", inventory.getCheapestPrice("Brazil", 5));

        assertTrue(brazil.availableInventory(95));
        assertTrue(argentina.availableInventory(100));
    }


    @Test
    public void shouldBeAbleToGetInventoryFromArgentina() {
        assertEquals("4500", inventory.getCheapestPrice("Brazil", 50));

        assertTrue(brazil.availableInventory(100));
        assertTrue(argentina.availableInventory(50));
    }

    @Test
    public void shouldBeAbleToGetOutOfStockWhenThereIsNoRequiredQuantity() {
        assertEquals("Out Of Stock", inventory.getCheapestPrice("Brazil", 500));
        assertTrue(brazil.availableInventory(100));
        assertTrue(argentina.availableInventory(100));
    }

    @Test
    public void shouldBeAbleToGetInventoriesFromMultipleCountries() {
        assertEquals("7800", inventory.getCheapestPrice("Argentina", 120));
        assertTrue(brazil.availableInventory(80));
        assertTrue(argentina.availableInventory(0));
    }

    @Test
    public void shouldBeAbleToNotifyOutOfStockWhenWeHaveInsufficientInventry() {
        assertEquals("Out Of Stock", inventory.getCheapestPrice("Argentina", 250));
        assertTrue(brazil.availableInventory(100));
        assertTrue(argentina.availableInventory(100));
    }

}
