package com.Inventory;

public class Country {
    private String countryName;
    private int quantity;
    private int pricePerUnit;
    private Inventory inventory;

    public Country(String countryName, int quantity, int pricePerUnit, Inventory inventory) {

        this.countryName = countryName;
        this.quantity = quantity;
        this.pricePerUnit = pricePerUnit;
        this.inventory = inventory;
    }


    public boolean availableInventory(int expectedInventory) {
        return (expectedInventory == quantity);
    }

    public int calculatePrice(String countryName, int itemNeeded) {
        if (itemNeeded > quantity) return Integer.MAX_VALUE;

        if (countryName.equals(this.countryName)) return itemNeeded * pricePerUnit;

        double shippingCharges = Math.ceil(itemNeeded / 10.0) * 400;
        return (int) ((itemNeeded * pricePerUnit) + shippingCharges);
    }

    public void updateInventory(int itemNeeded) {
        quantity = quantity - itemNeeded;
        inventory.updateInQuantity(itemNeeded);
    }

    public int addQuantity(int totalItem) {
        return totalItem + quantity;
    }

    public int quantityDifference(int itemNeeded) {
        return itemNeeded-quantity;
    }
}
