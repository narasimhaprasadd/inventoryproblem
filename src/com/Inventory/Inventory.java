package com.Inventory;

import java.util.ArrayList;
import java.util.List;

public class Inventory {
    List<Country> listOfCountries = new ArrayList<>();
    private int totalItem;
    int lowestCost;
    Country cheapestCountry;


    public void addCountry(Country country) {
        listOfCountries.add(country);
        totalItem = country.addQuantity(totalItem);
    }


    public String getCheapestPrice(String countryName, int itemNeeded) {
        cheapestCountry = null;
        lowestCost = Integer.MAX_VALUE;
        if (hasSufficientQuantity(itemNeeded)) {
            String price = getCheapest(countryName, itemNeeded);
            if (price.equals("Out Of Stock")) {
                return getCheapestByPartial(countryName, itemNeeded);
            }
            return price;
        }

        return "Out Of Stock";
    }


    private String getCheapestByPartial(String countryNameByUser, int itemNeeded) {
        Country brazil = listOfCountries.get(0);
        Country argentina = listOfCountries.get(1);

        int remainingRequiredQuantity = brazil.quantityDifference(itemNeeded);

        int firstPrice = getPriceForCountry(brazil, itemNeeded - remainingRequiredQuantity, countryNameByUser);
        firstPrice += getPriceForCountry(argentina, remainingRequiredQuantity, countryNameByUser);

        remainingRequiredQuantity = argentina.quantityDifference(itemNeeded);

        int secondPrice = getPriceForCountry(brazil, itemNeeded - remainingRequiredQuantity, countryNameByUser);
        secondPrice += getPriceForCountry(argentina, remainingRequiredQuantity, countryNameByUser);


        if (firstPrice < secondPrice) return String.valueOf(firstPrice);
        return String.valueOf(secondPrice);

    }


    private int getPriceForCountry(Country country, int itemNeeded, String countryName) {
        int remainingRequiredQuantity = country.quantityDifference(itemNeeded);
        int totalPrice = Integer.parseInt(getCheapest(countryName, itemNeeded - remainingRequiredQuantity));
        totalPrice += Integer.parseInt(getCheapest(countryName, remainingRequiredQuantity));
        return totalPrice;
    }


    private String getCheapest(String countryName, int itemNeeded) {
        for (Country country : listOfCountries) {
            int price = country.calculatePrice(countryName, itemNeeded);

            if (price < lowestCost) {
                lowestCost = price;
                cheapestCountry = country;
            }
        }

        if (cheapestCountry != null)
            updateCountryInventory(cheapestCountry, itemNeeded);

        if (lowestCost == Integer.MAX_VALUE) {
            return "Out Of Stock";
        }
        return String.valueOf(lowestCost);
    }

    private boolean hasSufficientQuantity(int itemNeeded) {
        return (totalItem >= itemNeeded);
    }


    private void updateCountryInventory(Country cheapestCountry, int itemNeeded) {
        cheapestCountry.updateInventory(itemNeeded);
    }

    public void updateInQuantity(int itemNeeded) {
        totalItem -= itemNeeded;
    }
}
